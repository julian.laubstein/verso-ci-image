FROM debian:buster-slim

ENV RUST_VERSION stable

RUN apt update -y -qq && \
    apt install -y -qq --no-install-recommends \
        git nodejs npm build-essential && \
    apt clean && \
    rm -fr /var/lib/lists/* /var/cache/* /usr/share/doc/* /usr/share/locale/*

RUN curl https://sh.rustup.rs -sSf | sh -s -- --default-toolchain $(RUST_VERSION) -y
ENV PATH ${HOME}/.cargo/bin:${PATH}
